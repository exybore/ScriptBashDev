#!/bin/bash
commands_help(){
clear

echo -e "\n
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
|                       Menu                      |
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
|                                                 |
|   1) GIT                                        |
|   2) NPM (in progress)                          |
|   3) COMPOSER (in progress)                     |                                       |
|   4) QUIT                                       |
|                                                 |
|                                                 |
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
|      Type command 'help' for show commands      |
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
\n"

}

commands_help

while [ 1 ]; do
    echo -n "Which program do you want to launch ? : "
    read choices

    case ${choices} in

    1 )

        clear
        bash ./bash_git/git.sh
    ;;

    2 )

        echo " in progress ;)"
    ;;

     3 )

        echo " in progress ;)"
    ;;


    help )
        commands_help ;;

    4 | quit | exit )

        clear
        echo "Thank you for using it, hoping it will help you !!"
        echo
        echo
        break
    ;;

    * )
        clear
        echo "Sorry, I don't found programs"
    ;;
    esac


done