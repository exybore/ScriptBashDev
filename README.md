# ScriptBashDev (SBD)



## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

See what software you will need to make the script functional :

* [GIT](https://git-scm.com) - Git is a free and open source distributed version control system
* [NPM](https://www.npmjs.com/) - Npm is the package manager for JavaScript
* [COMPOSER](https://getcomposer.org/) - Composer is a tool for dependency management in PHP



### Installing

In SSH :

```
git clone git@gitlab.com:BaptDu/ScriptBashDev.git
```

In HTTPS :

```
git clone https://gitlab.com/BaptDu/ScriptBashDev.git
```
## Wiki 

A wiki was established to explain each step of the project and will allow you to follow my learning on this technology, you can access it here : [Wiki](https://gitlab.com/BaptDu/ScriptBashDev/wikis/ScriptBashDev---WIKI)

## Authors

**Baptiste Dumont** - *Student in Web Development* - [Website](https://dumontbaptiste.fr/)