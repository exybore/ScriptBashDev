#!/bin/sh

review_history_log(){

  clear
  git log --pretty=oneline --abbrev-commit --graph
  echo
  echo -n "Do you want to know more [y/n] ? : "
  read history_reps

  if [ ${history_reps} = "y" ]; then

    git log --stat

  fi
}