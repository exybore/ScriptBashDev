#!/bin/sh

synchronize_changes_push(){

  clear
  echo -n "Do you want to push [y/n] ? : "
  read push_reps

  if [ ${push_reps} = "y" ]; then

      git push

  fi

}

synchronize_changes_pull(){

  clear
  echo -n "Do you want to pull [y/n] ? : "
  read pull_reps

  if [ ${pull_reps} = "y" ]; then

      git pull

  fi

}

synchronize_changes_merge(){

  clear
  echo -n "Do you want to merge branch [y/n] ? : "
  read merge_reps

  if [ ${merge_reps} = "y" ]; then

      clear
      echo -n "Which branch do you want to connect to ? : "
      read -e branchOne

      git checkout ${branchOne}

      echo -n "What branch do you want merge ? : "
      read -e branchTwo

      git merge ${branchTwo}
      git push

      clear
      echo -n "Delete your branch $branchTwo [y/n] ? : "
      read -e delete_reps

      if [ ${delete_reps} = "y" ]; then

      git branch -D ${branchTwo}

      fi

  fi

}