#!/bin/sh

create_repositories_clone(){

    clear
    cd ~
    echo "You are currently here: $(pwd)"
    echo -n "Where do you want to do it clone ? : "
    read -e clone_url

    cd ${clone_url}

    clear
    echo "You are here: $(pwd)"

    echo -n "Repository link : "
    read -e clone_reps

    git clone "$clone_reps"
}