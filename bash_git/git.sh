#!/bin/bash

# Call Functions
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$DIR/./functions/make_changes.sh"
source "$DIR/./functions/synchronize_changes.sh"
source "$DIR/./functions/create_repositories.sh"
source "$DIR/./functions/configure_tooling.sh"
source "$DIR/./functions/review_history.sh"
source "$DIR/./functions/commands.sh"

clear
echo "############################################################"
echo -e "###                Welcome to ScriptBashDev - \e[1mGit\e[0m        ###"
echo "############################################################"
echo "###   To have more information about available commands  ###"
echo -e "###                       Type : \e[1mcommands\e[0m                ###"
echo "############################################################"


while [ 1 ]; do

    echo
    echo
    echo -n "Which command do you want to launch ? : "
    read choices

    case ${choices} in

    config )

        configure_tooling_name
        configure_tooling_email
    ;;

    clone )

        create_repositories_clone
    ;;

    clone_remote )

        create_repositories_remote
    ;;

    status )

        make_changes_status
        make_changes_add
        make_changes_status
        make_changes_commit
        synchronize_changes_push
    ;;

    add )

        make_changes_add
        make_changes_status
        make_changes_commit
        synchronize_changes_push
    ;;

    commit )

        make_changes_commit
        synchronize_changes_push
    ;;

    push )

         synchronize_changes_push
    ;;

    merge )

        synchronize_changes_merge
    ;;

    reset )

        make_changes_reset
    ;;

    log )

        review_history_log
    ;;

    commands )

        clear
        commands_info

    ;;

    exit | quit )
        clear
        break
    ;;

    * )
        clear
        echo "Sorry, I don't found commands"
    ;;
    esac

done